/* Contiene los casos de test para el compilador C-TDS
 *
 */
#include <stdio.h>
#include "libtest.c"


// retorna el factorial de v, con v hasta 15
int factorial (int v){
     int limit;
    limit = 15;
    if ( v > limit){
        return -1;
    }
    {//nuevo bloque
         int c;
         int fact;
        c = 0;
        fact = 1;
        while (c<v){
            c = c+1;
            fact = fact*c;
        }
        return fact;
    }
}


// funcion main
int main (){

    int x;
    int i = 0;


    initinput();

    // test factorial entero
    printString(2);
    x= getInt() ; // lee la cantidad de veces que ejecutara la funcion
    i = 0;
    while (i<x){
        int aux;
        aux= getInt(); // lee los datos para invocar a la funcion
        printStringFactorialValor(aux); // imprime: Factorial(aux)=
        aux = factorial(aux);
        printInt(aux);
        i = i + 1;
    }
    printString(1);

    closeInput();
    return 0;


} // endmain

#ifndef _ANY_C
#define _ANY_C

#include "any.h"
#include <string.h>

int normalice(int n){
	return ((n/16)+1)*16;
}

void checkNodeNode(node* a, node* b){
	if (a->data->tipe == UNDEFINED || b->data->tipe == UNDEFINED)
		return;
	if (a->data->tipe != b->data->tipe){
		printf("ERROR --> linea %d : Los tipos no coinciden%s.%s\n",  b->data->line, a->data->name, b->data->name);
		exit(1);
	}
}

void checkNodeType(node* a, type b){
	if (a->data->tipe == UNDEFINED || b == UNDEFINED)
		return;
	if (a->data->tipe != b){
		printf("ERROR --> linea %d : Los tipos no coinciden.\n", a->data->line);
		exit(1);
	}
}

void checkTypeType(type a, type b, int line){
	if (a == UNDEFINED || b == UNDEFINED)
		return;
	if (a != b){
		printf("ERROR --> linea %d : Los tipos no coinciden.\n", line);
		exit(1);
	}
}

void checkNotType(type a, type not, int line){
	if (a == not){
		printf("ERROR --> linea %d : No puede ser de ese tipo.\n", line);
		exit(1);
	}
}

//generador de variables temporales
char* createId(char* prefijo, int id){
	char* x = malloc(sizeof(char)*(strlen(prefijo)+5));
	strcpy(x,prefijo);
	sprintf(x, "%s%d", x, id);
	return x;
}

//generador de variables temporales
char* createName(char* prefijo, char* name){
	char* x = malloc(sizeof(char)*(strlen(prefijo)+strlen(prefijo)));
	strcpy(x,prefijo);
	sprintf(x, "%s%s", x, name);
	return x;
}

#endif

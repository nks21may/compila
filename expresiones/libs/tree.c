#ifndef _TREE_C
#define _TREE_C

#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include "tree.h"

node* load_node(node* n, node* sl, node* sr, info* d, char* l){
	n->hi = sl;
	n->hd = sr;
	n->data = d;
	n->label = l;
	return n;
}

node* load_node_id(node* n, node* sl, node* sr, info* d, char* l, char* id){
	n->hi = sl;
	n->hd = sr;
	n->data = d;
	n->label = l;
	n->id = id;
	return n;
}

int resolve_tree(node* n){
	if(n->label == NULL){ //constante
		return n->data->val;
	}
	if(strcmp(n->label,"NEXT") == 0){ //siguiente instruccion
		if(n->hi != NULL)
			resolve_tree(n->hi);
		if(n->hd != NULL)
			resolve_tree(n->hd);

		return 0;
	}
	if(strcmp(n->label,"ASIG") == 0){ //asig
		n->hi->data->val = resolve_tree(n->hd);
		return 0;
	}
	if(strcmp(n->label,"PRINTI") == 0){ //asig
		printf("%d \n", resolve_tree(n->hd));
		return 0;
	}

	if(strcmp(n->label,"VAR") == 0){ //variable
		return n->data->val;
	}
	if (strcmp(n->label,"SUMA") == 0){
		return resolve_tree(n->hi) + resolve_tree(n->hd);
	}
	if (strcmp(n->label,"RESTA") == 0){
		return resolve_tree(n->hi) - resolve_tree(n->hd);
	}
	if (strcmp(n->label,"MULT") == 0){
		return resolve_tree(n->hi) * resolve_tree(n->hd);
	}
	if (strcmp(n->label,"DIV") == 0){
		if (resolve_tree(n->hd) == 0){
			printf("ERROR --> linea %d : Division por 0.\n", n->data->line);
			exit(3);
		}
		return resolve_tree(n->hi) / resolve_tree(n->hd);
	}
	if (strcmp(n->label,"MOD") == 0){
		if (resolve_tree(n->hd) == 0){
			printf("ERROR --> linea %d : Division por 0.\n", n->data->line);
			exit(4);
		}
		return resolve_tree(n->hi) / resolve_tree(n->hd);
	}
	printf("ERROR --> linea %d : Opcion no soportada.\n", n->data->line);
	exit(1);
}

int show_tree(node* n,int i){
	if (n == NULL)
		return 0;
	for (int k = 0; k < i; ++k){
		printf("   │");
	}
	if (n->label == NULL){
		printf("── %d \n", n->data->val);
	}else{
		if (strcmp(n->label,"VAR") == 0){
			if (n->data->tipe == BOOL) {
					printf("──D(%d) %s %s (%p) \n", n->data->deep, n->data->name, "BOOL", n->data);
			}
			if (n->data->tipe == INTEGER){
				printf("──D(%d) %s %s (%p) \n", n->data->deep, n->data->name, "INTEGER", n->data);
			}
			if (n->data->tipe == VOID){
				printf("──D(%d) %s %s (%p) \n", n->data->deep, n->data->name, "VOID", n->data);
			}
		}else{
			printf("── %s \n", n->label);
		}
	}
	show_tree(n->hi, i+1);
	show_tree(n->hd, i+1);
	return 0;
}

#endif

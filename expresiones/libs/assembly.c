#ifndef _ASSEMBLY_C
#define _ASSEMBLY_C

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include "assembly.h"
#include "tableFunctions.h"
#include "any.h"
#include "table.h"

FILE* f;

void aritmetic(inst op, info* a, info* b, info* c){
  //Mover el primer op a un registro; movl	-4(%rbp), %eax
  if (a != NULL){
    if (a->name != NULL){
      fprintf(f, "  movl -%d(%%rbp), %%eax\n", a->offset);
    }else{
      fprintf(f, "  movl $%d, %%eax \n", a->val);
    }
  }

  //Mover el seg op a otro registro; movl  -4(%rbp), %edx
  if (b != NULL && op != ASIG && op != PRINTI){
    if (op == DIV || op == MOD){
      if (b->name != NULL){
        fprintf(f, "  movl -%d(%%rbp), %%ecx \n", b->offset);
      }else{
        fprintf(f, "  movl $%d, %%ecx \n", b->val);
      }
    }else{
      if (b->name != NULL){
        fprintf(f, "  movl -%d(%%rbp), %%edx \n", b->offset);
      }else{
        fprintf(f, "  movl $%d, %%edx \n", b->val);
      }
    }
  }else{
    fprintf(f, "  movl $1, %%edx \n");
  }

  switch (op) {
    case ASIG: break;
    case SUMA: fprintf(f, "  addl %%edx, %%eax\n");
      break;
    case RESTA: fprintf(f, "  subl %%edx, %%eax\n");
          break;
    case MULT: fprintf(f, " imull %%edx, %%eax\n");
          break;
    case DIV: fprintf(f, "  cltd \n  idivl %%ecx\n");
          break;
    case MOD: fprintf(f, "  cltd \n  idivl %%ecx\n ");
          break;
    case PRINTI: if (a->tipe == BOOL){
                  fprintf(f, "  movl  %%eax, %%edi\n  call  printb\n");
                }else{
                  fprintf(f, "  movl	%%eax, %%edi\n  call	printi\n");
                }
          break;
    case ANDI: fprintf(f, "  and %%edx, %%eax\n");
          break;
    case MINI: fprintf(f, "  cmp %%eax, %%edx\n  movl $0, %%eax\n  movl $1, %%edx\n  cmovg %%edx, %%eax\n");
          break;
    case MAXI: fprintf(f, "  cmp %%eax, %%edx\n  movl $0, %%eax\n  movl $1, %%edx\n  cmovl %%edx, %%eax\n");
          break;
    case ORI: fprintf(f, "  or %%edx, %%eax\n");
          break;
    case EQI: fprintf(f, "  cmp %%eax, %%edx\n  movl $0, %%eax\n  movl $1, %%edx\n  cmove %%edx, %%eax\n");
          break;
    case NOTI: fprintf(f, "  xor %%eax, %%eax\n  test %%edx, %%edx\n  sete %%al \n");
          break;
    case JUMP: fprintf(f, "  jmp %s\n", c->name);
          break;
    case FJUMP: fprintf(f, "  cmp %%eax, %%edx \n  jne %s\n", c->name);
          break;
    case LABEL: fprintf(f, "%s:\n", c->name);
          break;
    default: printf("default in aritmetic\n");
      printf("....%d....\n", op);
      break;
  }
  //save in c
  if (op != PRINTI && op != FJUMP && op != JUMP && op != LABEL){
    if (op != MOD){
      fprintf(f, "  movl %%eax, -%d(%%rbp)\n", c->offset);
    }else{
      fprintf(f, "  movl %%edx, -%d(%%rbp)\n", c->offset);
    }
  }

  //clear eax; movl  $0, %eax
  //fprintf(f, "  movl $0, %%eax\n");
}

static const char init[] ="  .file \"%s\"\n  .text\n";
static const char end[] ="  .ident	\"GCC: (Debian 8.3.0-6) 8.3.0\"\n  .section	.note.GNU-stack,\"\",@progbits\n";

int registro = 0;
void arg(inst op, info* c){
  if(op != ARG) return;
  switch (registro) {
    case 0:   //	movl	$33, %edi
        if (c->name != NULL)
          fprintf(f, "  movl -%d(%%rbp), %%edi\n", c->offset);
        else
          fprintf(f, "  movl $%d, %%edi \n", c->val);
        registro = registro+1;
      break;
    case 1:  //	movl	$44, %esi
        if (c->name != NULL)
          fprintf(f, "  movl -%d(%%rbp), %%esi\n", c->offset);
        else
          fprintf(f, "  movl $%d, %%esi \n", c->val);
        registro = registro+1;
      break;
    case 2:  //	movl	$44, %edx
        if (c->name != NULL)
          fprintf(f, "  movl -%d(%%rbp), %%edx\n", c->offset);
        else
          fprintf(f, "  movl $%d, %%edx \n", c->val);
        registro = registro+1;
      break;
    case 3:  //	movl	$44, %ecx
        if (c->name != NULL)
          fprintf(f, "  movl -%d(%%rbp), %%ecx\n", c->offset);
        else
          fprintf(f, "  movl $%d, %%ecx \n", c->val);
        registro = registro+1;
      break;
    case 4:  //	movl	$44, %r8d
        if (c->name != NULL)
          fprintf(f, "  movl -%d(%%rbp), %%r8d\n", c->offset);
        else
          fprintf(f, "  movl $%d, %%r8d \n", c->val);
        registro = registro+1;
      break;
    case 5:  //	movl	$44, %r9d
        if (c->name != NULL)
          fprintf(f, "  movl -%d(%%rbp), %%r9d\n", c->offset);
        else
          fprintf(f, "  movl $%d, %%r9d \n", c->val);
        registro = registro+1;
      break;
    default: //pushq	$77777
        if (c->name != NULL)
          fprintf(f, "  pushq -%d(%%rbp)\n", c->offset);
        else
          fprintf(f, "  pushq $%d\n", c->val);
        registro = registro+1;
      break;
  }
  return;
}

void genCall(inst op, info* a, info* c){
  if(op != CALL) return;
  fprintf(f, "  movl $0, %%eax\n");
  fprintf(f, "  call %s\n", a->name);
  fprintf(f, "  movl %%eax, -%d(%%rbp) \n", c->offset);
  //fprintf(f, "  addq	$%d, %%rsp\n", normalice(registro*4));
  if (registro <= 5) {
    registro = 0;
    return;
  }
  registro = registro - 5;
  registro = 0;
}

void genReturn(instruc* op, int b){
  if(op->i != RET) return;
  if (op->op3 != NULL) {
    if (op->op3->name != NULL){
      fprintf(f, "  movl -%d(%%rbp), %%eax\n", op->op3->offset);
    }else{
      fprintf(f, "  movl $%d, %%eax \n", op->op3->val);
    }
  }
  if (b) {
    fprintf(f, "  movl $0, %%eax \n");
  }
  fprintf(f, "  leave\n");
  fprintf(f, "  ret\n");
}

void toAssembly(tableFunction* all){
  char* file_result = malloc(sizeof(char)*31);
  strncpy(file_result, file_name, strlen(file_name)-4);
  strcat(file_result, ".s");

  f = fopen(file_result, "w");

  if (f == NULL){
    printf("Error opening file!\n");
    exit(1);
  }

  fprintf(f, init,file_name);

  while(all != NULL){
    genFunc(all);
    all = all->sig;
  }
  fprintf(f, end);
  fclose(f);
}

int auxParam = 0;
int pila = 3;
void extracParam(instruc* op){
  if(op->i != PARAM) return;
  switch(auxParam){
    case 0:
        if (op->op3->name != NULL)
          fprintf(f, "  movl %%edi, -%d(%%rbp)\n", op->op3->offset);
        auxParam = auxParam+1;
      break;
    case 1:  // movl  $44, %esi
        if (op->op3->name != NULL)
          fprintf(f, "  movl %%esi, -%d(%%rbp)\n", op->op3->offset);
        auxParam = auxParam+1;
      break;
    case 2:  // movl  $44, %edx
        if (op->op3->name != NULL)
          fprintf(f, "  movl %%edx, -%d(%%rbp)\n", op->op3->offset);
        auxParam = auxParam+1;
      break;
    case 3:  // movl  $44, %ecx
        if (op->op3->name != NULL)
          fprintf(f, "  movl %%ecx, -%d(%%rbp)\n", op->op3->offset);
        auxParam = auxParam+1;
      break;
    case 4:  // movl  $44, %r8d
        if (op->op3->name != NULL)
          fprintf(f, "  movl %%r8d, -%d(%%rbp)\n", op->op3->offset);
        auxParam = auxParam+1;
      break;
    case 5:  // movl  $44, %r9d
        if (op->op3->name != NULL)
          fprintf(f, "  movl %%r9d, -%d(%%rbp)\n", op->op3->offset);
        auxParam = auxParam+1;
      break;
    default: //pushq  $77777
        fprintf(f, "  movl  %d(%%rbp), -%d(%%rbp)\n", pila*OFFSET_SIZE ,op->op3->offset);
        pila = pila+1;
      break;
  }
}

void genFunc(tableFunction* t){
  if (t->head == NULL) return;
  instruc* op = t->inst;
  if(op->i != FUNC) return;
  fprintf(f," .globl  %s\n .type %s, @function\n", t->name, t->name);
  fprintf(f, "%s:\n",op->op3->name);
  fprintf(f, "  pushq %rbp\n");
  fprintf(f, "  movq	%%rsp, %%rbp\n");
  fprintf(f, "  subq  $%d, %%rsp\n", normalice(t->off));
  op = op->sig;
  while(op->i == PARAM){
    extracParam(op);
    op = op->sig;
  }
  auxParam = 0;
  pila = 3;
  while(op != NULL){
    switch(op->i){
      case RET: genReturn(op, t->retorn == VOID);
          break;
      case CALL: genCall(op->i, op->op1, op->op3);
          break;
      case ARG: arg(op->i, op->op3);
        break;
      default: aritmetic(op->i, op->op1, op->op2, op->op3);
        break;
    }
    op = op->sig;
  }
}

#endif

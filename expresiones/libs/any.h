#ifndef _ANY_H
#define _ANY_H

#include <stdlib.h>
#include <stdio.h>
#include "structures.h"

/*
Esta libreria tiene todas las funciones que
aun no fueron asignadas a ningun otro archivo
*/

int normalice(int n);

void checkNodeNode(node* a, node* b);
void checkNodeType(node* a, type b);
void checkTypeType(type a, type b, int line);
void checkNotType(type a, type not, int line);

char* createId(char* prefijo, int id);
char* createName(char* prefijo, char* name);

#endif

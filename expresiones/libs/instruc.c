#ifndef _INSTRUC_C
#define _INSTRUC_C

#include "instruc.h"

instruc* load_inst(instruc* anterior, inst i, info* op1, info* op2, info* op3){
// printf("*load_inst %p\n", anterior);

	instruc* n = malloc(sizeof(instruc));
	anterior->sig = n;
	n->i = i;
	n->op1 = op1;
	n->op2 = op2;
	n->op3 = op3;
	return n;
}

static int id = 1;

//generador de variables temporales
info* createTempVar(type t){
//	printf("*createTempVar\n");
	char *x=malloc(sizeof(char)*5);
	sprintf(x, "t%d", id);
	id = id+1;
	info* f = load_info_name_with_offset(x, 0);
	f->tipe = t;
	return f;
}

instruc* generate_function_instruc(tableFunction* t, instruc* ant){
//	printf("*generate_function_instruc\n");
	if(t->head == NULL) return ant;
	instruc* ksi = malloc(sizeof(instruc));
	instruc* head = ksi;
	ksi->i = FUNC;
	ksi->op3 = load_info_name(t->name, 0);
	table* auxParam = t->params;
	setOffset(t->off+OFFSET_SIZE);
	while(auxParam != NULL){
		ksi->sig = malloc(sizeof(instruc));
		ksi = ksi->sig;
		ksi->i = PARAM;
		ksi->op3 = auxParam->data;
		auxParam = auxParam->sig;
	}

	instruc* end = gen_list_instruc(t->head, ksi);
	ant->sig = head;

	if (t->retorn == VOID && end->i != RET) {
		end->sig = malloc(sizeof(instruc));
		end = end->sig;
		end->i = RET;
	}
	t->off = getOffset();
	return end;
}

instruc* createInstructLabel(enum inst ins, node* n, instruc* list, type t){
//	printf("*createInstructLabel \n");
	instruc* auxhi = NULL;
	instruc* auxhd = NULL;
	instruc* hhi = malloc(sizeof(instruc));
	instruc* hhd = malloc(sizeof(instruc));
	info* infi = NULL;
	info* infd = NULL;
	if(n->hi != NULL){
		if (n->hi->label == NULL || (strcmp(n->hi->label,"VAR") == 0))
			infi = n->hi->data;
		else{
			auxhi = gen_list_instruc(n->hi, hhi);
			infi = auxhi->op3;
		}
	}
	if(n->hd != NULL){
		if (n->hd->label == NULL || (strcmp(n->hd->label,"VAR") == 0))
			infd = n->hd->data;
		else{
			auxhd = gen_list_instruc(n->hd, hhd);
			infd = auxhd->op3;
		}
	}
	if (auxhi != NULL){
		list->sig = hhi->sig;
		list = auxhi;
	}
	if (auxhd != NULL){
		list->sig = hhd->sig;
		list = auxhd;
	}
	if (ins != ARG) {
		return load_inst(list, ins, infi, infd, createTempVar(t));
	}
	return load_inst(list, ins, NULL, NULL, infi);
}

int id_jump = 0;

instruc* gen_list_instruc(node* n, instruc* list){
//	printf("*gen_list_instuc -> %s (%p)\n", n->label, list);
	instruc* aux = malloc(sizeof(instruc));
	if(strcmp(n->label,"NEXT") == 0){ //siguiente instruccion
		aux = list;
		if(n->hi != NULL)
			aux = gen_list_instruc(n->hi, list);
		if(n->hd != NULL)
			aux = gen_list_instruc(n->hd, aux);
		return aux;
	}

	if(strcmp(n->label,"ASIG") == 0){ //asig HD null HI
		if (n->hd->label == NULL || (strcmp(n->hd->label,"VAR") == 0)){
			info* infd;
			infd = n->hd->data;
			return load_inst(list, ASIG, infd, NULL, n->hi->data);
		}

		instruc* auxhd = gen_list_instruc(n->hd, list);
		return load_inst(auxhd, ASIG, auxhd->op3, NULL, n->hi->data);
	}

	if(strcmp(n->label,"PRINTI") == 0){
		if (n->hd->label == NULL || (strcmp(n->hd->label,"VAR") == 0)){
			info* infd;
			infd = n->hd->data;
			return load_inst(list, PRINTI, infd, NULL, NULL);
		}

		instruc* auxhd = gen_list_instruc(n->hd, list);
		return load_inst(auxhd, PRINTI, auxhd->op3, NULL, NULL);
	}

	if(strcmp(n->label,"RETURN") == 0){
		if(n->hi == NULL && n->hd == NULL) return load_inst(list, RET, NULL, NULL, NULL);
		if (n->hi->label == NULL || (strcmp(n->hi->label,"VAR") == 0)){
			info* infd;
			infd = n->hi->data;
			return load_inst(list, RET, NULL, NULL, infd);
		}

		instruc* auxhd = gen_list_instruc(n->hi, list);
		return load_inst(auxhd, RET, NULL, NULL, auxhd->op3);
	}

	if(strcmp(n->label,"ARG") == 0){ //asig HD null HI
		node* naux = n;
		node* naux2;
		aux = list;
		while(naux != NULL){
			naux2 = naux;
			naux = naux->hd;
			naux2->hd = NULL;
			aux = createInstructLabel(ARG, naux2, aux, UNDEFINED);
		}
		return aux;
	}

	if (strcmp(n->label,"CALL") == 0)
		return createInstructLabel(CALL, n, list, UNDEFINED);

	if (strcmp(n->label,"SUMA") == 0)
		return createInstructLabel(SUMA, n, list, INTEGER);
	if (strcmp(n->label,"RESTA") == 0)
		return createInstructLabel(RESTA, n, list, INTEGER);
	if (strcmp(n->label,"MULT") == 0)
		return createInstructLabel(MULT, n, list, INTEGER);
	if (strcmp(n->label,"DIV") == 0)
		return createInstructLabel(DIV, n, list, INTEGER);
	if (strcmp(n->label,"MOD") == 0)
		return createInstructLabel(MOD, n, list, INTEGER);

	if (strcmp(n->label,"MIN") == 0)
			return createInstructLabel(MINI, n, list, BOOL);
	if (strcmp(n->label,"MAX") == 0)
			return createInstructLabel(MAXI, n, list, BOOL);
	if (strcmp(n->label,"EQ") == 0)
			return createInstructLabel(EQI, n, list, BOOL);
	if (strcmp(n->label,"AND") == 0)
			return createInstructLabel(ANDI, n, list, BOOL);
	if (strcmp(n->label,"OR") == 0)
			return createInstructLabel(ORI, n, list, BOOL);
	if (strcmp(n->label,"NOT") == 0){
		if (n->hi->label == NULL || (strcmp(n->hi->label,"VAR") == 0)){
				info* infi;
				infi = n->hi->data;
				return load_inst(list, NOTI, NULL,infi, createTempVar(BOOL));
		}
		instruc* auxhi = gen_list_instruc(n->hi, list);
		return load_inst(auxhi, NOTI,  NULL, auxhi->op3, auxhi->op3);
	}
	if (strcmp(n->label,"WHILE") == 0){
		//crear label para volver
		info* auxInfo;
		auxInfo = malloc(sizeof(info));
		auxInfo->name = n->id;
		instruc* labelInicio = load_inst(list,LABEL, NULL, NULL, auxInfo);

		////////////////////////

		info* infi;
		instruc* hhi = malloc(sizeof(instruc));
		//condicion
		instruc* auxCondition = NULL;
		if (n->hi->label == NULL || (strcmp(n->hi->label,"VAR") == 0))
			infi = n->hi->data;
		else{
			auxCondition = gen_list_instruc(n->hi, hhi);
			infi = auxCondition->op3;
		}
		//lo une a la lista
		if (auxCondition != NULL){
			labelInicio->sig = hhi->sig;
			labelInicio = auxCondition;
		}

		//label del end
		info* auxEnd;
		auxEnd = malloc(sizeof(info));
		auxEnd->name = createId("endWhile", id_jump);
		id_jump = id_jump+1;

		aux = load_inst(labelInicio, FJUMP, infi, NULL, auxEnd);
		instruc* auxhd = gen_list_instruc(n->hd,aux);
		instruc* aux2 = load_inst(auxhd, JUMP, NULL, NULL, auxInfo);
		return load_inst(aux2, LABEL, NULL, NULL, auxEnd);
	}
	if (strcmp(n->label,"IF") == 0){
		info* auxInfo;
		auxInfo = malloc(sizeof(info));
		auxInfo->name = n->id;
		instruc* auxhi = NULL;
		instruc* hhi = malloc(sizeof(instruc));
		info* infi;

		//condicion
		if (n->hi->label == NULL || (strcmp(n->hi->label,"VAR") == 0))
			infi = n->hi->data;
		else{
			auxhi = gen_list_instruc(n->hi, hhi);
			infi = auxhi->op3;
		}
		//lo une a la lista
		if (auxhi != NULL){
			list->sig = hhi->sig;
			list = auxhi;
		}
		aux = load_inst(list, FJUMP, infi, NULL, auxInfo);
		instruc* auxhd = gen_list_instruc(n->hd,aux);

		return load_inst(auxhd, LABEL, NULL, NULL, auxInfo);
	}

	if (strcmp(n->label,"IFELSE") == 0){
		info* auxInfo;
		auxInfo = malloc(sizeof(info));
		auxInfo->name = n->id;
		instruc* auxhi = NULL;
		instruc* hhi = malloc(sizeof(instruc));
		info* infi;

		//condicion
		if (n->hi->label == NULL || (strcmp(n->hi->label,"VAR") == 0))
			infi = n->hi->data;
		else{
			auxhi = gen_list_instruc(n->hi, hhi);
			infi = auxhi->op3;
		}
		//lo une a la lista
		if (auxhi != NULL){
			list->sig = hhi->sig;
			list = auxhi;
		}
		aux = load_inst(list, FJUMP, infi, NULL, n->data); //salto por falso
		instruc* auxhd = gen_list_instruc(n->hd,aux); //sentencias


		return load_inst(auxhd, JUMP, NULL, NULL, auxInfo);
	}

	if (strcmp(n->label,"ELSE") == 0){
		info* auxInfo;
		auxInfo = malloc(sizeof(info));
		auxInfo->name = n->id;
		instruc* lab = load_inst(list, LABEL, NULL, NULL, n->data);

		instruc* auxhd = gen_list_instruc(n->hd,lab); //sentencias

		return load_inst(auxhd, LABEL, NULL, NULL, auxInfo);
	}

	return list;
}

// sintax: INSTRUCCION (^offsetOp1) op1 (^offsetOp2) op2 (^offsetOp3) op3
void priti(instruc* list){
	instruc* g = list;
	while(g != NULL){
		if (g->i == CALL){
			printf("CALL ");
		}
		if (g->i == FUNC){
			printf("FUNC ");
		}
		if (g->i == FFUNC){
			printf("FFUNC ");
		}
		if (g->i == PARAM){
			printf("PARAM ");
		}
		if (g->i == ARG){
			printf("ARG ");
		}
		if (g->i == RET){
			printf("RET ");
		}
		if (g->i == SUMA){
			printf("SUMA ");
		}
		if (g->i == RESTA){
			printf("RESTA ");
		}
		if (g->i == MULT){
			printf("MULT ");
		}
		if (g->i == DIV){
			printf("DIV ");
		}
		if (g->i == ASIG){
			printf("ASIG ");
		}
		if (g->i == PRINTI){
			printf("PRINTI ");
		}
		if (g->i == MINI){
			printf("MIN ");
		}
		if (g->i == MAXI){
			printf("MAX ");
		}
		if (g->i == EQI){
			printf("EQ ");
		}
		if (g->i == ANDI){
			printf("AND ");
		}
		if (g->i == ORI){
			printf("OR ");
		}
		if (g->i == NOTI){
			printf("NOT ");
		}
		if (g->i == JUMP){
			printf("JUMP ");
		}
		if (g->i == FJUMP){
			printf("FJUMP ");
		}
		if (g->i == LABEL){
			printf("LABEL ");
		}
		if (g->op1 == NULL){
			printf("NULL ");
		}else{
			printf("{(^%d) ", g->op1->offset);
			printf("[%d] ", g->op1->tipe);
			if (g->op1->name != NULL){
				printf("%s} ", g->op1->name);
			}
			else{
				printf("%d} ", g->op1->val);
			}
		}
		if (g->op2 == NULL){
			printf("NULL ");
		}else{
			printf("{(^%d) ", g->op2->offset);
			printf("[%d] ", g->op2->tipe);
			if (g->op2->name != NULL){
				printf("%s} ", g->op2->name);
			}else{
				printf("%d} ", g->op2->val);
			}
		}
		if (g->op3 == NULL){
			printf("NULL ");
		}else{
			printf("{(^%d) ", g->op3->offset);
			printf("[%d] ", g->op3->tipe);
			if (g->op3->name != NULL){
				printf("%s} ", g->op3->name);
			}else{
				printf("%d} ", g->op3->val);
			}
		}
		printf("\n");
		g = g->sig;
	}
}

#endif

#ifndef _TREE_H
#define _TREE_H

#include <stdlib.h>
#include <stdio.h>
#include "structures.h"

struct node* load_node(node* n, node* sl, node* sr, info* d, char* l);
struct node* load_node_id(node* n, node* sl, node* sr, info* d, char* l, char* id);

int resolve_tree(node* n);

int show_tree(node* n, int i);

#endif
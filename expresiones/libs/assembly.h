#ifndef _ASSEMBLY_H
#define _ASSEMBLY_H

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include "instruc.h"

void aritmetic(inst, info* a, info* b, info* c);

void arg(inst op, info* c);
void genCall(inst op, info* a, info* c);
void genReturn(instruc* op, int b);
void toAssembly(tableFunction* all);
void extracParam(instruc* op);
void genFunc(tableFunction* t);

#endif

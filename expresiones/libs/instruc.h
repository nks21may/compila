#ifndef _INSTRUC_H
#define _INSTRUC_H

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include "table.h"
#include "any.h"
#include "structures.h"


instruc* load_inst(instruc* n, inst i, info* op1, info* op2, info* op3);

info* createTempVar(type t);

instruc* createInstructLabel(enum inst ins, node* n, instruc* list, type t);

instruc* gen_list_instruc(node* n, instruc* list);

instruc* generate_function_instruc(tableFunction* t, instruc* ant);

void priti(instruc* list);

#endif

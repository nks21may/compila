#ifndef _TABLEFUNCTIONS_H
#define _TABLEFUNCTIONS_H

#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include "structures.h"
#include "table.h"
#include "tree.h"
#include "any.h"

struct tableFunction* search_in_tableFunction(tableFunction* head, char* x);

struct tableFunction* insert_in_tableFunction(tableFunction* head, tableFunction* x, int bool);

void printTableFunction(tableFunction* head);

void checkParamsCall(table* params, node* n, int line);

tableFunction* reverse(tableFunction* head, tableFunction* ant);

int siz(tableFunction* head);

#endif

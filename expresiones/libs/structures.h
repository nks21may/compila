#ifndef _STRUCTURES_H
#define _STRUCTURES_H
#define OFFSET_SIZE 4

char* file_name;

typedef enum type {INTEGER, BOOL, VOID,UNDEFINED}type;

typedef enum inst {ASIG,
	SUMA, RESTA, MULT, DIV, MOD,
	CALL, PRINTI,
	ANDI, ORI, MINI, MAXI, EQI, NOTI,
	JUMP, FJUMP, LABEL,
	FUNC, FFUNC, PARAM, RET, ARG}inst;

typedef struct info{
	char* name;
	int val;
	int line;
	int offset;
	int deep;
	enum type tipe;
}info;

typedef struct table{
	struct table* sig;
	info* data;
}table;

typedef struct node{
	struct node* hi;
	struct node* hd;
	char* label;
	char* id;
	info* data;
}node;

typedef struct instruc{
	enum inst i;
	info* op1;
	info* op2;
	info* op3;
	struct instruc* sig;
}instruc;

typedef struct tableFunction{
	struct tableFunction* sig;
	table* params;
	table* variables;
	char* name;
	node* head;
	type retorn;
	instruc* inst;
	int off;
	int foundReturn;
}tableFunction;

#endif

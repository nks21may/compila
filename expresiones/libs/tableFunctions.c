#ifndef _TABLEFUNCTIONS_C
#define _TABLEFUNCTIONS_C

#include "tableFunctions.h"

tableFunction* search_in_tableFunction(tableFunction* head, char* x){
	tableFunction* aux = head;
	while (aux != NULL){
		if(strcmp(aux->name, x) != 0)
			aux = aux->sig;
		else
			return aux;
	}
	return aux;
}

tableFunction* insert_in_tableFunction(tableFunction* head, tableFunction* x, int bool){
	tableFunction* a = search_in_tableFunction(head, x->name);
	if(a != NULL && bool){
		printf("ERROR --> Funcion %s duplicada.\n", x->name);
		exit(1);
	}
	if(a == NULL){
		x->sig = head;
		return x;
	}else{
		return a;
	}
}

void printTableFunction(tableFunction* head){
	if (head == NULL){
		printf("NULL \n");
		return;
	}
	printf("Funcion %s: %d\n", head->name, head->retorn);
	printf("Parametros: \n");
	printTable(head->params);

	printf("------------------------\n");
	printf("Arbol: \n");
	show_tree(head->head,0);
	printf("========================\n");
	printTableFunction(head->sig);
	return;
}

void checkParamsCall(table* params, node* n, int line){
	while(n != NULL && params != NULL){
		checkTypeType(n->hi->data->tipe, params->data->tipe, n->hi->data->line);
		params = params->sig;
		n = n->hd;
	}
	if (n != NULL || params != NULL) {
		show_tree(n,0);
		printf("ERROR linea %d --> La llamada a la funcion tiene distinta cantidad de parametros.\n",line);
		exit(1);
	}
}

tableFunction* reverse(tableFunction* head, tableFunction* ant){
	if(head == NULL) return NULL;
	if (head->sig == NULL){
		head->sig = ant;
		return head;
	}
	tableFunction* aux = head->sig;
	head->sig = ant;
	return reverse(aux, head);
}

int siz(tableFunction* head){
	if(head == NULL) return 0;
	return siz(head->sig) +1;
}

#endif

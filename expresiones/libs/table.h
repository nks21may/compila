#ifndef _TABLE_H
#define _TABLE_H

#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include "structures.h"

static int offs = OFFSET_SIZE;

struct info* load_info_val(int v, int line);

struct info* load_info_name(char* name, int line);

struct info* load_info_name_with_offset(char* name, int line);

struct info* update_val(info* i, int v);

struct table* search_in_list(table* head, info* id, int bool);

struct table* insert_in_list(table* head, info* id);

struct table* dropLevel(table* head, int level);

int printTable(table* head);

int getOffset();
void resetOffset();
void setOffset(int i);
int sizeTable(table* t);

#endif

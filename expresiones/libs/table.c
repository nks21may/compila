#ifndef _TABLE_C
#define _TABLE_C

#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include "table.h"

info* load_info_val(int v, int line){
	info* i = malloc(sizeof(info));
	i->val = v;
	i->line = line;
	return i;
}

info* load_info_name(char* name, int line){
	info* i = malloc(sizeof(info));
	i->name = name;
	i->line = line;
	return i;
}

info* load_info_name_with_offset(char* name, int line){
	info* i = load_info_name(name, line);
	i->offset = offs;
	offs = offs + OFFSET_SIZE;
	return i;
}

info* update_val(info* i, int v){
	if (i != NULL){
		i->val = v; //actualiza valor variable
	} else{
		printf("ERROR --> linea %d : Variable %s no +++++++++ definida.\n", i->line, i->name);
		exit(1);
	}
	return i;
}


table* search_in_list(table* head, info* id, int bool){
	table* aux = head;

	while (aux != NULL){
		if(strcmp(aux->data->name,id->name) != 0)
			aux = aux->sig;
		else
			return aux;
	}
	if (bool){
		if(aux == NULL){
			printf("ERROR --> linea %d : Variable %s no definida.\n", id->line, id->name);
			exit(1);
		}
	}
	return aux;
}

table* insert_in_list(table* head, info* id){
	table* a = search_in_list(head, id, 0);
	if(a != NULL && a->data->deep == id->deep){
		printf("ERROR --> linea %d : Variable %s ya definida.\n", id->line, id->name);
		exit(1);
	}
	table* l = malloc(sizeof(table));
	l->sig = head;
	l->data = id;
	l->data->offset = offs;
	offs = offs + OFFSET_SIZE;
	return l;
}

table* dropLevel(table* head, int level){
	while(1){ //warning while true
		if (head == NULL)
			break;
		if(head->data == NULL)
			break;
		if (head->data->deep != level)
			break;
		head = head->sig;
	}
	return head;
}

int printTable(table* head){
	if (head == NULL){
		printf("NULL \n");
		return 0;
	}
	if(head->data == NULL)
		printf("null data\n");

	printf("%s -> ", head->data->name);
	printTable(head->sig);
	return 0;
}

int getOffset(){
	return offs;
}

void resetOffset(){
	offs = OFFSET_SIZE;
}

void setOffset(int i){
	offs = i;
}

int sizeTable(table* t){
	int i = 0;
	while(t != NULL){
		i = i+1;
	}
	return i;
}

#endif

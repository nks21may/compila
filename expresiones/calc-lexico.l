%{

#include <stdlib.h>
#include <string.h>
#include <stdio.h>
#include "libs/structures.h"
#include "libs/table.h"
#include "libs/tree.h"
#include "libs/any.h"
#include "calc-sintaxis.tab.h"

%}

%option noyywrap
%option yylineno

letra [a-zA-Z]
digito [0-9]

%%

{digito}+       {
									yylval.nod = load_node(malloc(sizeof(node)), NULL, NULL, load_info_val(atoi(yytext), yylineno), NULL);
									yylval.nod->data->tipe = INTEGER;
                  return INT;
                }

"var"						{
									return VAR;
							  }

"main"					{return MAIN;}
"return"				{return RETURN;}
"extern"				{return EXTERN;}

"integer"				{
									yylval.t = INTEGER;
									return TYPE;
							  }

"bool"				  {
									yylval.t = BOOL;
									return TYPE;
							  }
"void"				  {
									yylval.t = VOID;
									return TYPE;
							  }

"TRUE"					{
									yylval.nod = load_node(malloc(sizeof(node)), NULL, NULL, load_info_val(1, yylineno), NULL);
									yylval.nod->data->tipe = BOOL;
									return TRUE;
							  }
"FALSE"				  {
									yylval.nod = load_node(malloc(sizeof(node)), NULL, NULL, load_info_val(0, yylineno), NULL);
									yylval.nod->data->tipe = BOOL;
									return FALSE;
							  }
"=="					{return EQ;}
"||"          {return OR;}
"&&"					{return AND;}

"while"				{return WHILE;}
"if"				  {return IF;}
"else"				{ return ELSE; }

"print"				{ return PRINT; }

{letra}({letra}|{digito})* 	{
															char* aux = strcpy(malloc(sizeof(yytext)*yyleng), yytext);
															yylval.nod = load_node(
															malloc(sizeof(node)), NULL, NULL,
															load_info_name(aux, yylineno), "VAR");
		                          return ID;
	                        	}

[-+*,!{}/<>=%;()]    { return *yytext;	}

"//".*\n            		  {}

.|\n                          ; /* ignore all the rest */

%%

void yyerror(){
	printf("%s%d\n","-> ERROR Sintactico en la linea: ",yylineno);
}

int main(int argc,char *argv[]){
	++argv,--argc;
	if (argc > 0)
		yyin = fopen(argv[0],"r");
	else
		yyin = stdin;

	file_name = malloc(sizeof(char)*40);
	strncpy(file_name, argv[0], strlen(argv[0]));
	yyparse();

}

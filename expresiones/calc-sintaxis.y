%{

#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include "libs/assembly.h"
#include "libs/table.h"
#include "libs/tableFunctions.h"
#include "libs/tree.h"
#include "libs/structures.h"
#include "libs/instruc.h"
#include "libs/any.h"

int deep = 0;
int i = 0;
char* retid = NULL;
tableFunction* currentFunction;
tableFunction* allFunction;

%}

%union {char *s; struct node* nod; struct tableFunction* fun; enum type t; struct table* tibli;}

%token<nod> INT
%token<s> VAR
%token<s> EXTERN
%token<s> RETURN
%token<s> MAIN
%token<s> AND
%token<s> OR
%token<s> EQ
%token<s> WHILE
%token<s> IF
%token<s> ELSE
%token<t> TYPE
%token<nod> PRINT
%token<nod> ID
%token<nod> TRUE
%token<nod> FALSE

%type<nod> expr
%type<nod> externs
%type<nod> ext
%type<nod> all
%type<nod> program
%type<nod> block
%type<fun> functions
%type<fun> function
%type<tibli> params
%type<nod> args
%type<fun> main
%type<nod> retur
%type<nod> declaration
%type<nod> declarations
%type<nod> statements
%type<nod> statement
%type<nod> conditionals

%left AND OR '!'
%left EQ
%left '+' '-'
%left '*' '/' '%'
%left '<' '>'

%%

all: externs functions {
//  printTableFunction(allFunction);
  //printf("Etapa 1\n");
  currentFunction = allFunction;
  //allFunction = reverse(allFunction, NULL);

  while(allFunction != NULL){
    instruc* a = malloc(sizeof(instruc));
    //printf("---*-----------------%s\n",allFunction->name);
    //show_tree(allFunction->head,0);
    generate_function_instruc(allFunction, a);
    allFunction->inst = a->sig;
    allFunction = allFunction->sig;
  }
  //printf("Etapa 2\n");
  toAssembly(currentFunction);
  /*gen_list_instuc($1, ksi);
  priti(ksi->sig);
  toAssembly(ksi->sig);*/
};

externs: externs ext{$$=NULL;}
  | {$$=NULL;};

ext: EXTERN TYPE ID{
        currentFunction = malloc(sizeof(tableFunction));
        currentFunction->retorn = $2;
        currentFunction->name = $3->data->name;
        allFunction = insert_in_tableFunction(allFunction, currentFunction, 1);
      }'('params')'';'{$$=NULL;};

functions: function functions {$$=NULL;}
  | main { allFunction = insert_in_tableFunction(allFunction, $1, 0);$$ = $1;$$=NULL;};

main: TYPE MAIN {
        currentFunction = malloc(sizeof(tableFunction));
        currentFunction->retorn = $1;
        currentFunction->name = "main";
        resetOffset();
      }
      block {
        currentFunction->head = $4;
        if(!currentFunction->foundReturn && currentFunction->retorn != VOID){
          printf("ERROR --> Funcion %s : Falta el return.\n",  currentFunction->name);
      		exit(1);
        }
        currentFunction->off = getOffset();
        $$ = currentFunction;
      };

function: TYPE ID {
        currentFunction = malloc(sizeof(tableFunction));
        currentFunction->retorn = $1;
        currentFunction->name = $2->data->name;
        tableFunction* u = search_in_tableFunction(allFunction, currentFunction->name);
        if(u != NULL)
          currentFunction = u;
        resetOffset();
      }
      '(' params ')'{
        setOffset(getOffset()+ OFFSET_SIZE);
        allFunction = insert_in_tableFunction(allFunction, currentFunction, 0);
      }
      block {

        currentFunction->head = $8;
        if(!currentFunction->foundReturn && currentFunction->retorn != VOID){
          printf("ERROR --> Funcion %s : Falta el return.\n",  currentFunction->name);
      		exit(1);
        }
        currentFunction->off = getOffset();
        $$ = currentFunction;

      };

params: params ',' TYPE ID {
                              checkNotType($3, VOID, $4->data->line);
                              $4->data->tipe = $3;
                              currentFunction->params = insert_in_list(currentFunction->params,$4->data);
                              currentFunction->params->data->offset = getOffset();
                              $$ = NULL;
                            }
  | TYPE ID {
                    checkNotType($1, VOID, $2->data->line);
                    $2->data->tipe = $1;
                    currentFunction->params = insert_in_list(currentFunction->params,$2->data);
                    currentFunction->params->data->offset = getOffset();
                    $$ = NULL;
                  }
  | {$$= NULL;};

block: '{' {deep = deep +1; } program retur'}' {
                      currentFunction->variables = dropLevel(currentFunction->variables, deep);
                      deep = deep - 1;
                      $$ = load_node(malloc(sizeof(node)), $3, $4, NULL, "NEXT");
                    };

program:  program statements {
                          $$ = load_node(malloc(sizeof(node)), $1, $2, NULL, "NEXT");
                          }
        | program conditionals {
                        $$ = load_node(malloc(sizeof(node)), $1, $2, NULL, "NEXT");
                        }
        | declarations {$$ = $1;};

retur: RETURN ';' {checkTypeType(VOID, currentFunction->retorn, 3333);
                   if(deep == 1)
                    currentFunction->foundReturn = deep;
                   $$ = load_node(malloc(sizeof(node)), NULL, NULL, NULL, "RETURN");
                 }
     | RETURN expr ';'{
                    if($2->data->tipe != VOID)
                      checkTypeType($2->data->tipe, currentFunction->retorn, 2222);

                    if(deep == 1)
                      currentFunction->foundReturn = deep;
                    $$ = load_node(malloc(sizeof(node)), $2, NULL, NULL, "RETURN");
                  }
      |{if(deep == 1) checkTypeType(VOID, currentFunction->retorn, 11111); $$=NULL;};

conditionals: WHILE '(' expr ')' block {
                                    checkNodeType($3, BOOL);
                                    i = i+1;
                                    $$ = load_node_id(malloc(sizeof(node)), $3, $5, NULL, "WHILE", createId("while", i));
                                  }
          | IF'('expr')' block {
                                checkNodeType($3, BOOL);
                                i = i+1;
                                $$ = load_node_id(malloc(sizeof(node)), $3, $5, NULL, "IF", createId("if", i));
                               }
          | IF'('expr')' block ELSE block{
                                checkNodeType($3, BOOL);
                                int ii = i;
                                info* di = malloc(sizeof(info));
                                i = i+1;
                                di->name = createId("else",ii);
                                node* izq = load_node_id(malloc(sizeof(node)), $3, $5, di, "IFELSE", createId("ifelse", i));
                                node* der = load_node_id(malloc(sizeof(node)), NULL, $7, di, "ELSE", izq->id);
                                $$ = load_node(malloc(sizeof(node)), izq, der, NULL, "NEXT");
                               }
          ;

declarations: declaration ';' declarations{

                  $$ = load_node(malloc(sizeof(node)), $1, $3, NULL, "NEXT");
              }
              | {$$ = NULL;};

declaration:  VAR TYPE ID   {
                              checkNotType($2, VOID, $3->data->line);
                              $3->data->tipe = $2;
                              $3->data->deep = deep;
                              currentFunction->variables = insert_in_list(currentFunction->variables,$3->data);
                              $$ = NULL;
                            }
            | VAR TYPE ID '=' expr {
                                checkNodeType($5, $2);
                                $3->data->tipe = $2;
                                $3->data->deep = deep;
                                currentFunction->variables = insert_in_list(currentFunction->variables,$3->data);
                                $$ = load_node(malloc(sizeof(node)), $3, $5, NULL, "ASIG");
                              };

statements:   statements ';' statement {
                                        $$ = load_node(malloc(sizeof(node)), $1, $3, NULL, "NEXT");
                                       }
            | statement ';' {
                          $$ = $1;
                        };

statement: ID '=' expr {
                          table* l = search_in_list(currentFunction->params,$1->data,0);
                          if(l == NULL){
                            l = search_in_list(currentFunction->variables,$1->data,1);
                          }
                          checkNodeType($3,l->data->tipe);
                          node* n = load_node(malloc(sizeof(node)), NULL, NULL, l->data, "VAR");
                          $$ = load_node(malloc(sizeof(node)), n, $3, NULL, "ASIG");
                        }
          | PRINT '(' expr ')' {
                                  $$ = load_node(malloc(sizeof(node)), NULL, $3, NULL, "PRINTI");
                                }
          | ID '(' args ')'   {
                                tableFunction* t = search_in_tableFunction(allFunction, $1->data->name);
                                if(t == NULL){
                                  printf("ERROR --> linea %d : La funcion %s no fue declarada.\n",  $1->data->line, $1->data->name);
                                  exit(1);
                                }
                                info* d = malloc(sizeof(info));
                                d->tipe = t->retorn;
                                checkParamsCall(t->params, $3, $1->data->line);
                                $1->data->name = t->name;
                                $$ = load_node(malloc(sizeof(node)), $1, $3, d, "CALL");
                              };

expr:
      expr '+' expr     {
                          checkNodeNode($1, $3);
                          info* d = malloc(sizeof(info));
                          d->tipe = INTEGER;
                          $$ = load_node(malloc(sizeof(node)), $1, $3, d, "SUMA");
                        }
    | expr '-' expr     {
                          checkNodeNode($1, $3);
                          info* d = malloc(sizeof(info));
                          d->tipe = INTEGER;
                          $$ = load_node(malloc(sizeof(node)), $1, $3, d, "RESTA");
                        }
    | expr '*' expr     {
                          checkNodeNode($1, $3);
                          info* d = malloc(sizeof(info));
                          d->tipe = INTEGER;
                          $$ = load_node(malloc(sizeof(node)), $1, $3, d, "MULT");
                        }
    | expr '/' expr     {
                          checkNodeNode($1, $3);
                          info* d = malloc(sizeof(info));
                          d->tipe = INTEGER;
                          $$ = load_node(malloc(sizeof(node)), $1, $3, d, "DIV");
                        }
    | expr '%' expr     {
                          checkNodeNode($1, $3);
                          info* d = malloc(sizeof(info));
                          d->tipe = INTEGER;
                          $$ = load_node(malloc(sizeof(node)), $1, $3, d, "MOD");
                        }
    | '(' expr ')'      {
                          $$ = $2;
                        }
    | '-' expr          {
                          checkNodeType($2, INTEGER);
                          info* in = malloc(sizeof(info));
                          in->val = -1;
                          in->tipe = INTEGER;
                          info* d = malloc(sizeof(info));
                          d->tipe = INTEGER;
                          node* n = load_node(malloc(sizeof(node)), NULL, NULL, in, NULL);
                          $$ = load_node(malloc(sizeof(node)), $2, n, d, "MULT");
                        }
    | ID '(' args ')'   {
                          tableFunction* t = search_in_tableFunction(allFunction, $1->data->name);
                          if(t == NULL){
                            printf("ERROR --> linea %d : La funcion %s no fue declarada.\n",  $1->data->line, $1->data->name);
                            exit(1);
                          }
                          info* d = malloc(sizeof(info));
                          d->tipe = t->retorn;
                          checkParamsCall(t->params, $3, $1->data->line);
                          $1->data->name = t->name;
                          $$ = load_node(malloc(sizeof(node)), $1, $3, d, "CALL");
                        }
    | ID                {
                          table* l = search_in_list(currentFunction->params,$1->data,0);
                          if(l == NULL){
                            l = search_in_list(currentFunction->variables,$1->data,1);
                          }
                          $$ = load_node(malloc(sizeof(node)), NULL, NULL, l->data , "VAR");
                        }
    | INT               {
                          $$ = $1;
                        }
    | TRUE              {
                          $$ = $1;
                        }
    | FALSE             {
                          $$ = $1;
                        }
    | expr '<' expr     {
                          checkNodeNode($1, $3);
                          info* d = malloc(sizeof(info));
                          d->tipe = BOOL;
                          $$ = load_node(malloc(sizeof(node)), $1, $3, d, "MIN");
                        }
    | expr '>' expr     {
                          checkNodeNode($1, $3);
                          info* d = malloc(sizeof(info));
                          d->tipe = BOOL;
                          $$ = load_node(malloc(sizeof(node)), $1, $3, d, "MAX");
                        }
    | expr EQ expr    {
                          checkNodeNode($1, $3);
                          info* d = malloc(sizeof(info));
                          d->tipe = BOOL;
                          $$ = load_node(malloc(sizeof(node)), $1, $3, d, "EQ");
                        }
    | expr AND expr    {
                          checkNodeNode($1, $3);
                          info* d = malloc(sizeof(info));
                          d->tipe = BOOL;
                          $$ = load_node(malloc(sizeof(node)), $1, $3, d, "AND");
                        }
    | expr OR expr    {
                          checkNodeNode($1, $3);
                          info* d = malloc(sizeof(info));
                          d->tipe = BOOL;
                          $$ = load_node(malloc(sizeof(node)), $1, $3, d, "OR");
                        }
    | '!' expr          {
                          checkNodeType($2, BOOL);
                          info* d = malloc(sizeof(info));
                          d->tipe = BOOL;
                          $$ = load_node(malloc(sizeof(node)), $2, NULL, d, "NOT");
                        }
    ;

args: expr {
            info* d = malloc(sizeof(info));
            d->tipe = $1->data->tipe;
            $$ = load_node(malloc(sizeof(node)), $1, NULL, d, "ARG");
          }
  | args ',' expr {
                    info* d = malloc(sizeof(info));
                    d->tipe = $3->data->tipe;
                    $$ = load_node(malloc(sizeof(node)), $3, $1, d, "ARG");
                  }
  | {$$ = NULL;};

%%

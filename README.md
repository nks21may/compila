# Taller de Diseño de Software (3306)
### Proyecto (año 2019)

***D'alessandro, Nicolás***


## _Aclaraciones_  
* El compilador acepta returns en toda la profundidad de la funcion pero obligatoriamente debe tener uno al final de la misma.
* No inicializa las variables con valores por defecto. Por lo que si uno no las inicializa utilizara la basura que lea.
* Los id son solamente caracteres y numeros. No soporta " _ " o " - ".
* 📌 *Se adjunta carpeta **libs**, que contiene librerias creadas para representar y manipular la tabla de simbolos y el arbol.*
** 📌 **structures.h**: se definen las estructuras y enumerados utilizados.
** 📌 **instruc.c**: contiene las funciones para la generacion del codigo 3-dir.
** 📌 **any.c**: contiene las funciones que no tienen un archivo propio.
** 📌 **assembly.c**: contiene funciones que pasan de codigo 3-dir assembler.
** 📌 **table.c**: contiene funciones para utilizar la lista de *info*.
** 📌 **tableFunctions.c**: contiene funciones para utilizar la lista de funciones.
** 📌 **tree.c**: contiene funciones para el manejo del arbol.
* 📌 Se adjunta carpeta **test**, con varios archivos de texto para tests. Estos tests pueden ser ejecutados con el script **run_all_test**.


## _Gramática_

    all: externs functions

    externs: externs ext
      | {}

    ext: EXTERN TYPE ID '(' params ')' ';'

    functions: function functions
      | main

    main: TYPE MAIN block

    function: TYPE ID '(' params ')' block

    params: params ',' TYPE ID
      | TYPE ID
      | {}

    block: '{' program retur '}'

    program: program statements
      | program conditionals
      | declarations

    retur: RETURN ';'
      | RETURN expr ';'
      | {}

    conditionals: WHILE '(' expr ')' block
      | IF '(' expr ')' block
      | IF '(' expr ')' block ELSE block

    declarations: declaration ';' declarations
      | {}

    declaration: VAR TYPE ID
      | VAR TYPE ID '=' expr

    statements: statements ';' statement
      | statement ';'

    statement: ID '=' expr
            | PRINT '(' expr ')'
            | ID '(' args ')'

    expr: expr '+' expr
      | expr '-' expr
      | expr '*' expr
      | expr '/' expr
      | expr '%' expr
      | '(' expr ')'
      | '-' expr
      | ID '(' args ')'
      | ID
      | INT
      | TRUE
      | FALSE
      | expr '<' expr
      | expr '>' expr
      | expr EQ expr
      | expr AND expr
      | expr OR expr
      | '!' expr

    args: expr
      | args ',' expr
      | {}


(1) **Tabla con referencias de el arbol:**

|  		label 	  |   Referencia   |   Hijo Izquierdo  |    Hijo Derecho   |
|-----------------|----------------|-------------------|-------------------|
| 	   "NEXT"     | salto de linea |   linea actual	   |  siguiente linea  |
| 	   "ASIG"     |   asignacion   |nombre de variable |  valor a asignar  |
| 	   "SUMA"     |      suma 	   | exp a la izq de + | exp a la der de + |
| 	   "RESTA"    |     resta 	   | exp a la izq de - | exp a la der de - |
| 	   "MULT"     | multiplicacion | exp a la izq de * | exp a la der de * |
| 	   "DIV"      |    division    | exp a la izq de / | exp a la der de / |
| 	   "MOD"      |     modulo     | exp a la izq de % | exp a la der de % |
| 	   "CALL"      |     llamada a funcion     | nombre de la func | parametros |
| 	   "MIN"      |     Minimo entre 2 expr     | exp a la izq | exp a la der |
| 	   "MAX"      |     Maximo entre 2 expr | exp a la izq | exp a la der |
| 	   "EQ"      |     = logico     | exp a la izq del = | exp a la der de = |
| 	   "AND"      |     And logico     | exp a la izq del and | exp a la der de and |
| 	   "OR"      |     Or logico     | exp a la izq del or | exp a la der de or |
| 	   "NOT"      |     negador logico     | exp a la izq del negador | null |
| 	   "VAR"      |   variable	   |	 		 NULL			   |			 NULL			   |
| 	  "ARG"       | argumento      |       argumento   | siguente argumento|
| 	  "PRINTI"    | func imprimir  |       NULL        |   exp a imprimir  |
| 	  "IFELSE"    | if con else  |       condicion        |   sentencias  |
| 	  "ELSE"    |  else del if  |       NULL        |   sentencias  |
| 	  "WHILE"    | Mientras  |       condicion        |   sentencias  |
| 	  "RET"    | Retorno  |      expresion que retorna o NULL         |  NULL  |

### Comandos utiles

```bash
./script; ./a.out input.txt;gcc -c input.s;gcc input.o libs/printi.c; ./a.out

./script ; mv a.out teacher\ tests ; cd teacher\ tests ; ./a.out testCTDS.tds ; gcc -c testCTDS.s ; gcc testCTDS.o libtest.c; ./a.out ; ..
```

### Interpretacion del arbol
El arbol se imprime Pre-order. A continuacion se muestra la forma en que se imprimen los distintos nodos junto con un ejemplo.
**Variables:** D(<profundidad>) <id> <type> (<memory address>). Ejemplo: D(2) z BOOL (0x56018c73e820)


### Creacion de tests
Para agregar test hay que seguir los siguentes pasos.

1.	Crear una carpeta con el nombre del nuevo test en `test/`.
2.	Agregar en esa carpeta un archivo `input.txt` con la entrada.
3.	Agregar en esa carpeta un archivo `output_expected.txt` con la salida.
4.	Agregar en esa carpeta un archivo `error_expected.txt` con la salida (si no espera un error, crearlo vacio).
